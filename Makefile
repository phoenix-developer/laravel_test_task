docker-up:
	docker-compose up -d

docker-down:
	docker-compose down

docker-build:
	docker-compose up --build -d

test:
	docker-compose exec php-cli vendor/bin/phpunit

db-migrate:
	docker-compose exec php-cli php artisan migrate

composer-install:
	docker-compose exec php-cli php composer.phar install

assets-install:
	docker-compose exec node yarn install

assets-dev:
	docker-compose exec node yarn run dev

assets-watch:
	docker-compose exec node yarn run watch

queue:
	docker-compose exec php-cli php artisan queue:work

link-storage:
	docker-compose exec php-cli php artisan storage:link

create-admin:
	docker-compose exec php-cli php artisan user:create-admin

init: docker-up composer-install db-migrate create-admin link-storage assets-install assets-dev queue
