<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->string("name",255);
            $table->string("email",255);
            $table->string("status",16);
            $table->string("image")->nullable();
            $table->boolean("edited")->default(false);
            $table->boolean("notify")->default(false);
            $table->text("message");
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
