<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(["register"=>false]);

Route::get('/reviews', 'ReviewsController@index');

Route::post('/reviews', 'ReviewsController@store');

Route::group([
    'as' => 'admin.',
    'namespace' => 'Admin',
    'middleware' => ['auth']
],function()
{
    Route::get("admin",'HomeController@index')->name('home');

    Route::resource("admin/reviews","ReviewsController");
});
