<?php

namespace App\Http\Controllers;

use App\Http\Requests\Reviews\CreateRequest;
use App\Jobs\Review\TelegramNotifyJob;
use App\Models\Review;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;

class ReviewsController extends Controller
{
    public function index()
    {
        $reviews = Review::orderBy('created_at','desc')
            ->where('status', '=', Review::STATUS_ACCEPT)
            ->paginate(5);

        $defaultPhoto = Review::$defaultPhoto;

        return view('reviews.index',compact('reviews','defaultPhoto'));
    }

    public function store(CreateRequest $request)
    {
        $data = $request->only([
            'name',
            'email',
            'message',
        ]);

        $data['status'] = Review::STATUS_NEW;

        $review = Review::create($data);

        if($request->hasFile('image'))
        {
            $file = $request->file('image');

            $image = Image::make($file->getRealPath());
            $image->resize(320, 240,function (Constraint $constraint)
            {
                $constraint->aspectRatio();
            });

            $fileName = sprintf('%s.%s',$review->id, $file->getClientOriginalExtension());

            $image->save(
                public_path('storage/reviews/'.$fileName)
            );

            $image->destroy();

            $review->image = '/storage/reviews/'. $fileName;
        }
        else
        {
            $review->image = Review::$defaultPhoto;
        }

        $review->save();

        $job = (new TelegramNotifyJob($review->id))->delay(5);

        $this->dispatch($job);

        return ['success' => true];
    }
}
