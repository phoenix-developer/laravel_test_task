<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\ModelFilter\Filters\Review\FindCreatedAtFrom;
use App\Contracts\ModelFilter\Filters\Review\FindCreatedAtTo;
use App\Contracts\ModelFilter\Filters\Review\FindEmail;
use App\Contracts\ModelFilter\Filters\Review\FindName;
use App\Contracts\ModelOrder\Orders\Review\OrderByCreatedAt;
use App\Contracts\ModelOrder\Orders\Review\OrderByEmail;
use App\Contracts\ModelOrder\Orders\Review\OrderByName;
use App\Helpers\Admin\Review\OrderDirectionHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Reviews\UpdateRequest;
use App\Models\Review;

class ReviewsController extends Controller
{
    public function index(OrderDirectionHelper $orderHelper)
    {
        $reviews = Review::withOrders($this->orders())
            ->withFilters($this->filters())
            ->paginate(20)
            ->appends(request()->all());

        $statusList = Review::statusList();

        return view('admin.reviews.index', compact('reviews','orderHelper','statusList'));
    }

    public function show(Review $review)
    {
        $statusList = Review::statusList();

        return view('admin.reviews.show',compact('review','statusList'));
    }

    public function edit(Review $review)
    {
        $statusList = Review::statusList();

        if($review->status !== Review::STATUS_NEW)
        {
            unset($statusList[Review::STATUS_NEW]);
        }

        return view('admin.reviews.edit',compact('review','statusList'));
    }

    public function update(UpdateRequest $request, Review $review)
    {
        $data = [
            'name' => $request['name'],
            'email' => $request['email'],
            'message' => $request['message'],
            'status' => $request['status'],
        ];

        if($review->isEditedByAdmin($review,$request))
        {
            $data['edited'] = true;
        }

        $review->update($data);

        return redirect()->route('admin.reviews.show',compact('review'));
    }

    public function filters()
    {
        return array(
            'find_created_at_from' => new FindCreatedAtFrom(),
            'find_created_at_to' => new FindCreatedAtTo(),
            'find_email' => new FindEmail(),
            'find_name' => new FindName()
        );
    }

    public function orders()
    {
        return array(
            'created_at' => new OrderByCreatedAt(),
            'name' => new OrderByName(),
            'email' => new OrderByEmail()
        );
    }
}
