<?php

namespace App\Jobs\Review;

use App\Models\Review;
use DomainException;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TelegramNotifyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;

    protected $token = '1163175384:AAHTrSobppZmVwkAZ6RAZJG2mGu0LKqCrF8';
    protected $chatId = '-1001394981676';

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function send(Review $review)
    {
        $url = sprintf("https://api.telegram.org/bot%s/sendPhoto?chat_id=%s",$this->token,$this->chatId);

        $message = [
            "Добавлен новый отзыв!",
            "Имя: {$review->name}",
            "Email:{$review->email}",
        ];

        $postFields = array(
            'chat_id' => $this->chatId,
            'photo' => new \CURLFile(public_path($review->image)),
            'caption' => implode("\n",$message)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:multipart/form-data"
        ));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        $response = json_decode(curl_exec($ch));

        curl_close($ch);

        return $response->ok;
    }

    public function handle()
    {
        $review = Review::find($this->id);

        if($review->notify)
        {
            throw new \Exception("Review is already notified");
        }

        if($this->send( $review ))
        {
            $review->notify = true;

            $review->save();
        }
        else
        {
            throw new \Exception("Error on telegram side");
        }
    }
}
