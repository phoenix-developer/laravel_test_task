<?php

namespace App\Contracts\ModelFilter;

use Illuminate\Database\Eloquent\Builder;

interface ModelFilterScope
{
    public function apply(Builder $builder,$filterScope);
}