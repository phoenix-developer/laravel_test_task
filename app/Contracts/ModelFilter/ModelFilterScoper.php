<?php

namespace App\Contracts\ModelFilter;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ModelFilterScoper
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply(Builder $builder,$scopes)
    {
        foreach($this->limitScopes($scopes) as $key => $scope)
        {
            if(!$scope instanceof ModelFilterScope)
            {
                continue;
            }

            $value = $this->request->get($key);

            if($value)
            {
                $scope->apply($builder, $value);
            }
        }
    }

    public function limitScopes(array $scopes)
    {
        return Arr::only($scopes,array_keys($this->request->all()));
    }
}