<?php

namespace App\Contracts\ModelFilter\Filters\Review;

use App\Contracts\ModelFilter\ModelFilterScope;
use Illuminate\Database\Eloquent\Builder;

class FindName implements ModelFilterScope
{
    public function apply( Builder $builder, $value )
    {
        return $builder->where('name','like', '%' . $value . '%');
    }
}