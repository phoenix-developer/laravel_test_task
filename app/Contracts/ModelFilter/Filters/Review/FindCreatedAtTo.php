<?php

namespace App\Contracts\ModelFilter\Filters\Review;

use App\Contracts\ModelFilter\ModelFilterScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class FindCreatedAtTo implements ModelFilterScope
{
    public function apply( Builder $builder, $value )
    {
        return $builder->whereDate('created_at','<=', Carbon::parse($value)->toDateString());
    }
}