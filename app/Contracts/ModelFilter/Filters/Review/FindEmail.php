<?php

namespace App\Contracts\ModelFilter\Filters\Review;

use App\Contracts\ModelFilter\ModelFilterScope;
use Illuminate\Database\Eloquent\Builder;

class FindEmail implements ModelFilterScope
{
    public function apply( Builder $builder, $value )
    {
        return $builder->where('email','like', '%' . $value . '%');
    }
}