<?


namespace App\Contracts\ModelOrder;


use Illuminate\Database\Eloquent\Builder;

interface ModelOrderScope
{
    public function apply(Builder $builder,$direction);
}