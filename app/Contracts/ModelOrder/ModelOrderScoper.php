<?php

namespace App\Contracts\ModelOrder;

use App\Contracts\ModelOrder\Orders\Review\OrderByCreatedAt;
use DomainException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ModelOrderScoper
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply(Builder $builder,$orders): bool
    {
        $orderScope = $this->limitOrder($orders);

        if(!$orderScope instanceof ModelOrderScope)
        {
            throw new DomainException('$orderScope is not instance of App\Contracts\ModelOrder\ModelOrderScope');
        }

        $orderScope->apply($builder,$this->getDirection());

        return true;
    }

    public function limitOrder($orders): ModelOrderScope
    {
        if($this->request->order_by && array_key_exists($this->request->order_by, $orders))
        {
            return $orders[$this->request->order_by];
        }
        else
        {
            return new OrderByCreatedAt();
        }
    }

    public function getDirection(): string
    {
        return $this->request->direction == 'asc' ? 'asc' : 'desc';
    }
}