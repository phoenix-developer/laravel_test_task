<?php

namespace App\Contracts\ModelOrder\Orders\Review;

use App\Contracts\ModelOrder\ModelOrderScope;
use Illuminate\Database\Eloquent\Builder;

class OrderByName implements ModelOrderScope
{
    public function apply( Builder $builder, $direction )
    {
        return $builder->orderBy('name',$direction);
    }
}