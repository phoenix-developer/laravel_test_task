<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CreateAdminCommand extends Command
{
    protected $signature = 'user:create-admin';

    protected $description = 'Creates a user named Admin with password ***';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try
        {

            User::create([
                'name' => 'Admin',
                'password' => '$2y$12$V9OKGcUD9ehS/THk5YiJD.XaqjrwbheVNKuRfNmpBpo0wMHnCmlR2',
                'email' => 'admin@test.by'
            ]);

        }
        catch (\DomainException $e)
        {
            $this->error($e->getMessage());
            return false;
        }

        $this->info('user `Admin` was created successfully');
    }
}
