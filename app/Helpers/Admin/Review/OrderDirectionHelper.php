<?php

namespace App\Helpers\Admin\Review;

use Illuminate\Http\Request;

class OrderDirectionHelper
{
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function makeRequest(string $column): array
    {
        $direction = 'desc';

        if( $this->isCurrentOrder($column) && $this->isDescDirection())
        {
            $direction = 'asc';
        }

        return array_merge($this->request->all(),['order_by' => $column , 'direction' => $direction]);
    }

    public function isCurrentOrder($column): bool
    {
        if(!$this->request->order_by && 'created_at' === $column)
        {
            return true;
        }

        return $this->request->order_by === $column;
    }

    public function isDescDirection(): bool
    {
        return !$this->request->direction || $this->request->direction === 'desc';
    }
}