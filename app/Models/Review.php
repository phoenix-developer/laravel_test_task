<?php

namespace App\Models;

use App\Http\Requests\Reviews\UpdateRequest;
use App\Models\Traits\CanBeFiltered;
use App\Models\Traits\CanBeOrdered;
use App\Models\Traits\DateFormat;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use DateFormat;
    use CanBeFiltered;
    use CanBeOrdered;

    protected $fillable = ['name','email','message','status','edited','notify','image'];

    public static $defaultPhoto = '/storage/reviews/no-photo.png';

    public const STATUS_NEW = 'new';
    public const STATUS_ACCEPT = 'accept';
    public const STATUS_REJECT = 'reject';

    public static function statusList(): array
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_ACCEPT => 'Принят',
            self::STATUS_REJECT => 'Отклонен',
        ];
    }

    public function getDefaultPhotoAttribute()
    {
        return static::$defaultPhoto;
    }

    public function isEditedByAdmin(Review $old,UpdateRequest $new)
    {
        $compare = [
            'name',
            'message',
            'email'
        ];

        foreach($compare as $field)
        {
            if($old->{$field} !== $new->{$field})
            {
                return true;
            }
        }

        return false;
    }
}
