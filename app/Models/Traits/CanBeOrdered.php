<?php

namespace App\Models\Traits;

use App\Contracts\ModelOrder\ModelOrderScoper;
use Illuminate\Database\Eloquent\Builder;

trait CanBeOrdered
{
    public function scopeWithOrders(Builder $builder,$orders = [])
    {
        $scoper = new ModelOrderScoper(request());

        $scoper->apply($builder, $orders);
    }
}