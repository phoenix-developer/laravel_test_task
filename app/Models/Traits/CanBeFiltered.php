<?php

namespace App\Models\Traits;

use App\Contracts\ModelFilter\ModelFilterScoper;
use Illuminate\Database\Eloquent\Builder;

trait CanBeFiltered
{
    public function scopeWithFilters(Builder $builder,$scopes = [])
    {
        $scoper = new ModelFilterScoper(request());

        $scoper->apply($builder, $scopes);
    }
}