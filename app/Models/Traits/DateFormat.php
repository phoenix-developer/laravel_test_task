<?php

namespace App\Models\Traits;

trait DateFormat
{
    public function getFormatCreatedAtAttribute()
    {
        if(isset($this->created_at))
        {
            return \Carbon\Carbon::parse($this->created_at)->format('d.m.Y H:i:s');
        }
    }
}