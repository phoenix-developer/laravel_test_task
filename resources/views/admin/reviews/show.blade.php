@extends('layouts.admin')

@section('content')

    <div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.reviews.edit', $review) }}" class="btn btn-primary mr-1">Редактировать</a>
    </div>

    <table class="table table-bordered table-striped">
        <tbody>
            <tr class="row no-gutters">
                <th class="col-6 px-3">ID</th><th class="col-6 px-3">{{$review->id}}</th>
            </tr>
            <tr class="row no-gutters">
                <th class="col-6 px-3">Фото</th>
                <th class="col-6 px-3">
                    <img class="admin-image" src="{{$review->image}}">
                </th>
            </tr>
            <tr class="row no-gutters">
                <th class="col-6 px-3">Имя пользователя</th><th class="col-6 px-3">{{$review->name}}</th>
            </tr>
            <tr class="row no-gutters">
                <th class="col-6 px-3">Email</th><th class="col-6 px-3">{{$review->email}}</th>
            </tr>
            <tr class="row no-gutters">
                <th class="col-6 px-3">Сообщение</th><th class="col-6 px-3">{{$review->message}}</th>
            </tr>
            <tr class="row no-gutters">
                <th class="col-6 px-3">Статус</th><th class="col-6 px-3">{{$statusList[$review->status]}}</th>
            </tr>
        </tbody>
    </table>
@endsection