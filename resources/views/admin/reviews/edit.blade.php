@extends('layouts.admin')

@section('content')

    <form method="POST" action="{{ route('admin.reviews.update', $review) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name" class="col-form-label">Имя пользователя</label>
            <input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $review->name) }}" required>
            @if ($errors->has('name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="email" class="col-form-label">Email</label>
            <input id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email', $review->email) }}" required>
            @if ($errors->has('email'))
                <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="message" class="col-form-label">Сообщение</label>
            <input id="message" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" value="{{old('message', $review->message) }}" required>
            @if ($errors->has('message'))
                <span class="invalid-feedback"><strong>{{ $errors->first('message') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="status" class="col-form-label">Статус</label>
            <select id="status" class="form-control" name="status">
                @foreach ($statusList as $value => $label)
                    <option value="{{ $value }}"{{ $value === old('status', $review->status) ? ' selected' : '' }}>{{ $label }}</option>
                @endforeach;
            </select>

            @if ($errors->has('status'))
                <div class="alert alert-danger mt-3"><strong>{{ $errors->first('status') }}</strong></div>
            @endif

        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@endsection