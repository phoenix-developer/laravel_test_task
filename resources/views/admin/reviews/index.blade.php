@extends('layouts.admin')

@section('content')

    <div class="card mb-3">
        <div class="card-header">Фильтр</div>
        <div class="card-body">
            <form action="?" method="GET">
                <input type="hidden" name="order_by" value="{{ request('order_by') }}">
                <input type="hidden" name="direction" value="{{ request('direction') }}">
                <input type="hidden" name="page" value="{{ request('page') }}">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Имя пользователя</label>
                            <input id="name" class="form-control" name="find_name" value="{{ request('find_name') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="email" class="col-form-label">Email</label>
                            <input id="email" class="form-control" name="find_email" value="{{ request('find_email') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="datetime_created_at_from" class="col-form-label">Дата создания от</label>
                            <date-input-component :name="'find_created_at_from'" value="{{ request('find_created_at_from')}}"></date-input-component>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="datetime_created_at_to" class="col-form-label">Дата создания до</label>
                            <date-input-component :name="'find_created_at_to'" value="{{ request('find_created_at_to') }}"></date-input-component>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br />
                            <button type="submit" class="btn btn-primary mr-3">Найти</button>
                            <button type="reset" class="btn btn-primary">Очистить</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card mb-3">
        <div class="card-body">
            Найдено записей: {{$reviews->total()}}
        </div>
    </div>

    <table class="table table-bordered table-striped table-responsive-sm">
        <thead>
            <tr>
                <th>ID</th>
                <th>Фото</th>
                <th>
                    <a href="{{route('admin.reviews.index',$orderHelper->makeRequest("name"))}}">
                        Имя пользователя
                        @if($orderHelper->isCurrentOrder('name') && $orderHelper->isDescDirection())
                            <i class="fa fa-angle-down"></i>
                        @else
                            <i class="fa fa-angle-up"></i>
                        @endif
                    </a>
                </th>
                <th>
                    <a href="{{route('admin.reviews.index',$orderHelper->makeRequest("email"))}}">
                        Email
                        @if($orderHelper->isCurrentOrder('email') && $orderHelper->isDescDirection())
                            <i class="fa fa-angle-down"></i>
                        @else
                            <i class="fa fa-angle-up"></i>
                        @endif
                    </a>
                </th>
                <th>Сообщение</th>
                <th>Статус</th>
                <th>
                    <a href="{{route('admin.reviews.index',$orderHelper->makeRequest("created_at"))}}">
                        Дата создания
                        @if($orderHelper->isCurrentOrder('created_at') && $orderHelper->isDescDirection())
                            <i class="fa fa-angle-down"></i>
                        @else
                            <i class="fa fa-angle-up"></i>
                        @endif
                    </a>
                </th>
            </tr>
        </thead>
        <tbody>

        @foreach ($reviews as $review)
            <tr>
                <td>{{ $review->id }}</td>
                <td>
                    <div class="admin-image-handler">
                        <img class="admin-image" src="{{$review->image}}">
                    </div>
                </td>
                <td>
                    <a href="{{ route('admin.reviews.show', $review) }}">
                        {{ $review->name }}
                    </a>
                    @if($review->edited)
                        <div class="alert alert-primary mt-2" role="alert">
                            Изменен администратором
                        </div>
                    @endif
                </td>
                <td>{{ $review->email }}</td>
                <td>{{ $review->message }}</td>
                <td>
                    {{ $statusList[$review->status] }}
                </td>
                <td>{{ $review->format_created_at }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>

    {{ $reviews->links() }}
@endsection