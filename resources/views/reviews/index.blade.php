@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7">
                <reviews-parent :items='@json($reviews)' :default-photo="'{{$defaultPhoto}}'">
                    <p slot="title">
                        Отзывы <span class="text-secondary">{{$reviews->total()}}</span>
                    </p>
                    <div slot="links">
                        {{ $reviews->links() }}
                    </div>
                </reviews-parent>
            </div>
        </div>
    </div>
@endsection