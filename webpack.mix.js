const mix = require('laravel-mix');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	.js('resources/js/app.js', 'public/js')
	.sass('resources/sass/app.scss', 'public/css');


mix.version();

mix.webpackConfig({
	module: {
		rules: [{
			test: /\.js?$/,
			exclude: /(node_modules)/,
			use: [{
				loader: 'babel-loader',
				options: mix.config.babel()
			}]
		}]
	}
});

mix.webpackConfig({
	output: {
		chunkFilename: 'js/chunks/[name].js',
	},
});